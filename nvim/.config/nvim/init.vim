set expandtab
set shiftwidth=2
set listchars=eol:↵,trail:~,tab:>-,nbsp:␣
set number
set relativenumber
set termguicolors
set showmatch

set modeline

" smartcase
set ignorecase
set smartcase
