export GPG_TTY=$(tty)

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
#if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
#fi

export ZCONFIG=$HOME/.config/zsh
ZPLUGIN=$ZCONFIG/plugins

setopt autocd extendedglob nomatch menucomplete
setopt interactivecomments
setopt prompt_subst

unsetopt BEEP

autoload -Uz compinit
zmodload zsh/complist

source $ZCONFIG/alias
source $ZCONFIG/theme
source $HOME/.profile

bindkey -e
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word

autoload -Uz history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

bindkey '^[[A' history-beginning-search-backward-end
bindkey '^[[B' history-beginning-search-forward-end

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=cyan'
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
source $ZPLUGIN/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZPLUGIN/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source $ZPLUGIN/powerlevel10k/powerlevel10k.zsh-theme

HISTFILESIZE=1000000000000
HISTSIZE=100000000000
SAVEHIST=100000000000
HISTFILE="$HOME/.config/zsh/history"
#setopt append_history
setopt extended_history
#setopt hist_ignore_all_dups
#setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history

autoload -U compinit
compinit
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
# [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
export POWERLEVEL9K_DISABLE_GITSTATUS=true
export GPG_TTY=$(tty)

precmd () {print -Pn "\e]0;%~\a"}
