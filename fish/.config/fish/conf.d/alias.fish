alias ls      "ls --color=always -h"
alias ls="ls --color=always -h"
#abbr -a l       "ls -la"
alias l="ls -la"
alias kitten  "kitty +kitten"
alias icat    "kitten icat"
#abbr -a diff    "kitten diff"
alias t       task
#abbr -a ip      "ip -brief"
#abbr -a vim      "vi"
#abbr -a in      'task add +in'
alias in="task add +in"

# vim: set syn=sh :
